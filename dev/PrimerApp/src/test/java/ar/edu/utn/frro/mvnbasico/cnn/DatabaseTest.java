package ar.edu.utn.frro.mvnbasico.cnn;

import java.sql.SQLException;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DatabaseTest extends TestCase {

	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */
	public DatabaseTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(DatabaseTest.class);
	}

	/**
     * 
     */
	public void testConnection() {
		List<String> nombres = null;
		try {
			MySQLConnection mysqlConnection = new MySQLConnection();
			nombres = mysqlConnection.getProductos();
		} catch (SQLException ex) {
			System.out.println("algo paso.. " + ex.getMessage());
		}
		assertTrue(nombres != null && !nombres.isEmpty());
	}
}
