package ar.edu.utn.frro.mvnbasico;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.commons.lang.StringUtils;

public class ArrayTest extends TestCase {

	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */
	public ArrayTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(ArrayTest.class);
	}

	/**
     * 
     */
	public void testCSV() {
		List<String> ids = new ArrayList<String>();
		ids.add("1");
		ids.add("2");
		ids.add("3");
		ids.add("4");
		String csv = StringUtils.join(ids, ",");
		Assert.assertEquals("1,2,3,4", csv);
	}
}
