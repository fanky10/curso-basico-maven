package ar.edu.utn.frro.mvnbasico;

import java.sql.SQLException;
import java.util.List;

import ar.edu.utn.frro.mvnbasico.cnn.MySQLConnection;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) throws SQLException {
		System.out.println("Primer-App");
		List<String> nombres = new MySQLConnection().getProductos();
		for (String nombre : nombres) {
			System.out.println("producto: " + nombre);
		}
	}
}
