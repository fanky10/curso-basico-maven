package ar.edu.utn.frro.mvnbasico.cnn;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class DatabaseConnection {
	protected Connection connection;

	protected abstract void createConnection() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException;

	private Connection getConnection() {
		try {
			if (connection == null) {
				createConnection();
			}
			return connection;
		} catch (Exception ex) {
			throw new IllegalArgumentException(
					"Algo no se encuentra bien configurado: " + ex.getMessage());
		}
	}

	public List<String> getProductos() throws SQLException {
		List<String> nombres = new ArrayList<String>();
		final String query = "SELECT nombre FROM productos";
		Statement stmt = null;
		try {
			stmt = getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				nombres.add(rs.getString("nombre"));
			}
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return nombres;
	}
}
