package ar.edu.utn.frro.mvnbasico.cnn;

import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection extends DatabaseConnection {
	private static final String URL = "jdbc:mysql://localhost/";
	private static final String NOMBRE_BDD = "mvnbasico_db";
	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final String USUARIO_BDD = "mvnbasico";
	private static final String PASS_BDD = "root";

	protected void createConnection() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName(MYSQL_DRIVER).newInstance();
		connection = DriverManager.getConnection(URL + NOMBRE_BDD, USUARIO_BDD,
				PASS_BDD);
	}

}
