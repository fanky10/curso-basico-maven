DROP DATABASE IF EXISTS mvnbasico_db;
CREATE DATABASE mvnbasico_db;
GRANT ALL PRIVILEGES ON mvnbasico_db.* to 'mvnbasico'@'localhost' identified by 'root';
connect mvnbasico_db;

CREATE TABLE productos (
	id_producto integer unsigned not null primary key AUTO_INCREMENT,
	nombre varchar(100) not null
);

